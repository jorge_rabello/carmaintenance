package br.com.jorgerabellodev.carm.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.jorgerabellodev.carm.daos.CarroDao
import br.com.jorgerabellodev.carm.daos.UsuarioDao
import br.com.jorgerabellodev.carm.domain.models.Carro
import br.com.jorgerabellodev.carm.domain.models.Servico
import br.com.jorgerabellodev.carm.domain.models.Usuario

@Database(entities = [(Servico::class), (Usuario::class), (Carro::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDAO(): UsuarioDao

    abstract fun carroDAO(): CarroDao

//    abstract fun servicoDAO(): ServicoDao
}
