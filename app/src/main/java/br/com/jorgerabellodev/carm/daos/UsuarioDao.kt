package br.com.jorgerabellodev.carm.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import br.com.jorgerabellodev.carm.domain.models.Usuario

@Dao
interface UsuarioDao {

    @Query("SELECT * FROM usuario WHERE email=:email AND senha=:senha")
    fun login(email: String, senha: String): Usuario

    @Insert
    fun addUser(usuario: Usuario)

    @Delete
    fun deleteUser(usuario: Usuario)
}
