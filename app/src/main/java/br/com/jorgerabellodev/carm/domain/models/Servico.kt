package br.com.jorgerabellodev.carm.domain.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.math.BigDecimal
import java.time.LocalDate

@Entity(
    foreignKeys = [ForeignKey(
        entity = Usuario::class,
        parentColumns = arrayOf("usuario_id"),
        childColumns = arrayOf("usuario_id"),
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    ), ForeignKey(
        entity = Carro::class,
        parentColumns = arrayOf("carro_id"),
        childColumns = arrayOf("carro_id"),
        onDelete = ForeignKey.CASCADE,
    )],
    tableName = "servico"
)
data class Servico(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "servico_id") val id: Long,
    @ColumnInfo(name = "data_inicio") val dataPrevistaInicio: String,
    @ColumnInfo(name = "data_entrega") val dataPrevistaEntrega: String,
    @ColumnInfo(name = "valor") val valor: Float,
    @ColumnInfo(name = "responsavel") val responsavel: String,
    @ColumnInfo(name = "usuario_id") val usuarioId: Long,
    @ColumnInfo(name = "carro_id") val carroId: Long
)
