package br.com.jorgerabellodev.carm.domain.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = Usuario::class,
        parentColumns = arrayOf("usuario_id"),
        childColumns = arrayOf("usuario_id"),
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )],
    tableName = "carro"
)
data class Carro(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "carro_id") val id: Long,
    @ColumnInfo(name = "modelo") val modelo: String,
    @ColumnInfo(name = "placa") val placa: String,
    @ColumnInfo(name = "cor") val cor: String,
    @ColumnInfo(name = "ano") val ano: Int,
    @ColumnInfo(name = "usuario_id") val usuarioId: Long
)
