package br.com.jorgerabellodev.carm.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.jorgerabellodev.carm.domain.models.Servico

@Dao
interface ServicoDao {

    @Query("SELECT * FROM servico WHERE usuario_id=:userId")
    fun findAllServicesByUserId(userId: Long): List<Servico>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addJob(servico: Servico)
}
