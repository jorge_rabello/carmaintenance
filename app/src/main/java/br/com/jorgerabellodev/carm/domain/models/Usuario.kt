package br.com.jorgerabellodev.carm.domain.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "usuario")
class Usuario(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "usuario_id") val usuarioId: Long,
    @ColumnInfo(name = "nome") val nome: String,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "senha") val senha: String
)
