package br.com.jorgerabellodev.carm.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.jorgerabellodev.carm.domain.models.Carro

@Dao
interface CarroDao {

    @Query("SELECT * FROM carro WHERE usuario_id=:usuarioId")
    fun findAllCarsByUserId(usuarioId: Long): List<Carro>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addCar(carro: Carro)
}
